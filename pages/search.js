import Api from "../Api.js";
import { asyncProvider } from "../loader.js";
import { layout } from "./popular.js";
import { renderMovies } from "./popular.js";
import { events } from "./popular.js";

const root = document.getElementById('app');

export const Search = async () => {
  const [, find] = window.location.search.split("?query=");
  root.innerHTML = layout;

  const film = await asyncProvider(async () => await Api.fetchMoviesBySearchText(find));
  let page = 1;//film.total_pages;
  
  const list = document.getElementById("list")
  const results = document.createElement("div"); 
  results.innerHTML = `Results: ${film.total_results}, Pages: ${film.total_pages}`
  list.prepend(results);
  
  renderMovies(film.results);

  if (film.total_pages > page) {
    const loadMoreButton = document.createElement("button");
    loadMoreButton.textContent = "Load More";
    list.append(loadMoreButton);

    loadMoreButton.addEventListener("click", async () => {
      page = page + 1;
      const newFilm = await asyncProvider(async () => await Api.fetchMoviesBySearchText(`${find}&page=${page}`));
      
      renderMovies(newFilm.results);
      list.append(loadMoreButton);

      if (newFilm.total_pages == page) {
        loadMoreButton.remove();
      }
     
    });
  } 

  events();
};